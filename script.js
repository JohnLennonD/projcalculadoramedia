// Button Adicionar
let buttonAdd = document.querySelector('.buttonAdd');
buttonAdd.addEventListener("click", takeValue);

// Button Calcular Média
let buttonCalc = document.querySelector('.buttonCalc');
buttonCalc.addEventListener("click", calcMedia);

var values = []

function takeValue() {
    let nota = document.querySelector('#valor').value
    if (nota == '') {
        alert("Por favor, insira uma nota")
    }else if (nota < 0 || nota > 10) {
        alert("A nota digitada é inválida, por favor, insira uma nota válida")
    }else {
        values.push(nota)
        let acc = values.length
        let txtArea = document.querySelector('#txtArea');
        let message = `A nota ${acc} foi ${nota}\n`
        txtArea.innerHTML += message 
        document.querySelector('#valor').value = ''
    }
};

function calcMedia() {
    let valor = 0
    for (value of values) {
        value = parseFloat(value)
        valor += value
    }

    let media = (valor / values.length)
    let spanValor = document.createElement('span')
    spanValor.innerText = media.toFixed(2)
    let pMedia = document.querySelector('#pMedia')
    pMedia.append(spanValor)

}